<?php
class {ClassName} extends CDbMigration
{
	private $sql = array();

	public function up()
	{
		$this->sql[]=<<<SQL
SELECT 1;
SQL;

		$this->executeSql();
	}

	public function down()
	{
		echo "{ClassName} does not support migration down.\\n";
		return false;
	}

	private function executeSql()
	{
		if (!empty($this->sql)) {
			foreach ($this->sql as $sql) {
				$this->execute($sql);
			}
		}
	}
}