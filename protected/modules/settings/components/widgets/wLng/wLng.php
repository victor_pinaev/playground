<?php
class wLng extends Widget
{
	public function init()
	{
		parent::init(__CLASS__);

		$this->controller->jsInit('settingsGeneral', 'initLng');
	}

	public function run()
	{
		$model = Users::model()->findByPk(Yii::app()->user->id);

		return $this->render('index', array('model'=>$model));
	}

	public static function actions()
	{
		Yii::setPathOfAlias(__CLASS__, realpath(dirname(__FILE__)));
		return array(
//			'reload'=>array('class'=>'application.components.WidgetBaseAction', 'widget_alias'=>__CLASS__),
			'save'=>array('class'=>__CLASS__.'.actions.Save', 'widget_alias'=>__CLASS__),
		);
	}
}