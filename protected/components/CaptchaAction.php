<?php
class CaptchaAction extends CCaptchaAction
{
	// Пришлось переопределять из-за отсутствия в оригинале одной строки: header('Content-type: application/json');
	public function run()
	{
		if(isset($_GET[self::REFRESH_GET_VAR]))  // AJAX request for regenerating code
		{
			$code=$this->getVerifyCode(true);
			header('Content-type: application/json');
			echo CJSON::encode(array(
					'hash1'=>$this->generateValidationHash($code),
					'hash2'=>$this->generateValidationHash(strtolower($code)),
					// we add a random 'v' parameter so that FireFox can refresh the image
					// when src attribute of image tag is changed
					'url'=>$this->getController()->createUrl($this->getId(),array('v' => uniqid())),
				));
		}
		else
			$this->renderImage($this->getVerifyCode());
		Yii::app()->end();
	}

	/**
	 * Переопрежделенный метод в котором добавлены цифры в процесс генерации.
	 * @return string
	 */
	protected function generateVerifyCode()
	{
		if($this->minLength > $this->maxLength)
			$this->maxLength = $this->minLength;
		if($this->minLength < 3)
			$this->minLength = 3;
		if($this->maxLength > 20)
			$this->maxLength = 20;
		$length = mt_rand($this->minLength,$this->maxLength);

		$letters = 'bcdfghjklmnpqrstvwxyz123456789';
		$vowels = 'aeiou';
		$code = '';
		for($i = 0; $i < $length; ++$i)
		{
			if($i % 2 && mt_rand(0,10) > 2 || !($i % 2) && mt_rand(0,10) > 9)
				$code.=$vowels[mt_rand(0,4)];
			else
				$code.=$letters[mt_rand(0,29)];
		}

		return $code;
	}
}