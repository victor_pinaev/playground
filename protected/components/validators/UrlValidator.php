<?php
class UrlValidator extends CUrlValidator
{
	public $isSwipe = false;
	public $allowEmpty = true;

	protected function validateAttribute($object, $attribute)
	{
		if($this->allowEmpty && $this->isEmpty($object->$attribute))
			return;

		if ($this->isSwipe) {
			$sp = new SwipeProcessor($object->$attribute);
			$value = $sp->getSwipeLink();
		} else {
			$value = $object->$attribute;
		}

		if ($this->allowEmpty && $this->isEmpty($value))
			return;
		if (($value = parent::validateValue($value)) === false) {
			$message = $this->message !== null ? $this->message : Yii::t('app','{attribute} contains invalid link. The link may contain only English letters');
			$this->addError($object,$attribute,$message);
		}
	}
}