<?php
class LoginForm extends CFormModel
{
	public $username;
	public $password;
	public $verifyCode;
	public $enableCaptcha = true;
	public $fpToken;
	public $fpToken2;
	public $skipLogging = false;
	public $prompt = '';

	private $_identity;

	public function rules()
	{
		return array(
			array('verifyCode, username, password, fpToken, fpToken2', 'HackStringValidate', 'skipOnError'=>true),
			array('verifyCode', 'captcha', 'allowEmpty'=>!LoginsLog::isCaptchaShown() || !CCaptcha::checkRequirements() || !$this->enableCaptcha, 'skipOnError'=>true, 'message'=>'We don\'t have a user with this email and password combination') ,
			array('username', 'filter', 'filter'=>'trim'),
			array('username, password, fpToken, fpToken2', 'required', 'skipOnError'=>true, 'message'=>'We don\'t have a user with this email and password combination'),
			array('fpToken', 'numerical', 'skipOnError'=>true, 'integerOnly' => true),
			array('fpToken2', 'length', 'skipOnError'=>true, 'max' => 32),
			array('password', 'skipOnError'=>true, 'authenticate'),
		);
	}

	public function afterValidate()
	{
		parent::afterValidate();

		if ($this->hasErrors()) {
			// логирование неудачных попыток входа в систему
			if (!$this->skipLogging) {
				$model = new LoginsLog;
				if (isset($this->_identity->userModel)) {
					$model->id_user = $this->_identity->userModel->id;
					PSift::login($this->_identity->userModel, false);
				}
				$model->fingerprint = $this->fpToken;
				$model->fingerprint2 = $this->fpToken2;
				$model->is_success = 0;
				$model->save();
			}
		} else {
			// регенерация капчи при успехе валидации
			if ($this->enableCaptcha)
				Yii::app()->getController()->createAction('captcha')->getVerifyCode(true);
		}
	}

	public function attributeLabels()
	{
		return array(
			'username' => 'Email',
			'password' => 'Password',
			'fpToken' => 'Login token',
		);
	}

	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			if(!$this->_identity->authenticate())
				$this->addError('password','We don\'t have a user with this email and
		password combination');
		}
	}

	public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$this->_identity->setState('fpToken', $this->fpToken);
			$this->_identity->setState('fpToken2', $this->fpToken2);
			Yii::app()->user->allowAutoLogin = true;
			Yii::app()->user->login($this->_identity, 3600*24*UserIdentityKeys::COOKIE_LOGIN_TTL);
			return true;
		}
		else
			return false;
	}
}
